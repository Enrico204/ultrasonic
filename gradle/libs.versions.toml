[versions]
# You need to run ./gradlew wrapper after updating the version
gradle                 = "8.1.1"

navigation             = "2.5.3"
gradlePlugin           = "8.0.2"
androidxcore           = "1.10.1"
ktlint                 = "0.43.2"
ktlintGradle           = "11.4.0"
detekt                 = "1.23.0"
preferences            = "1.2.0"
media3                 = "1.0.2"

androidSupport         = "1.6.0"
materialDesign         = "1.9.0"
constraintLayout       = "2.1.4"
multidex               = "2.0.1"
room                   = "2.5.1"
kotlin                 = "1.8.22"
kotlinxCoroutines      = "1.7.1"
viewModelKtx           = "2.6.1"
swipeRefresh           = "1.1.0"

retrofit               = "2.9.0"
## KEEP ON 2.13 branch (https://github.com/FasterXML/jackson-databind/issues/3658#issuecomment-1312633064) for compatibility with API 24
jackson                = "2.13.5"
okhttp                 = "4.11.0"
koin                   = "3.3.2"
picasso                = "2.8"

junit4                 = "4.13.2"
junit5                 = "5.9.3"
mockito                = "5.3.1"
mockitoKotlin          = "5.0.0"
kluent                 = "1.73"
apacheCodecs           = "1.15"
robolectric            = "4.10.3"
timber                 = "5.0.1"
fastScroll             = "2.0.1"
colorPicker            = "2.2.4"
rxJava                 = "3.1.6"
rxAndroid              = "3.0.2"
multiType              = "4.3.0"

[libraries]
gradle                  = { module = "com.android.tools.build:gradle", version.ref = "gradlePlugin" }
kotlin                  = { module = "org.jetbrains.kotlin:kotlin-gradle-plugin", version.ref = "kotlin" }
ktlintGradle            = { module = "org.jlleitschuh.gradle:ktlint-gradle", version.ref = "ktlintGradle" }
detekt                  = { module = "io.gitlab.arturbosch.detekt:detekt-gradle-plugin", version.ref = "detekt" }

core                    = { module = "androidx.core:core-ktx", version.ref = "androidxcore" }
design                  = { module = "com.google.android.material:material", version.ref = "materialDesign" }
annotations             = { module = "androidx.annotation:annotation", version.ref = "androidSupport" }
multidex                = { module = "androidx.multidex:multidex", version.ref = "multidex" }
constraintLayout        = { module = "androidx.constraintlayout:constraintlayout", version.ref = "constraintLayout" }
room                    = { module = "androidx.room:room-compiler", version.ref = "room" }
roomRuntime             = { module = "androidx.room:room-runtime", version.ref = "room" }
roomKtx                 = { module = "androidx.room:room-ktx", version.ref = "room" }
viewModelKtx            = { module = "androidx.lifecycle:lifecycle-viewmodel-ktx", version.ref = "viewModelKtx" }
navigationFragment      = { module = "androidx.navigation:navigation-fragment", version.ref = "navigation" }
navigationUi            = { module = "androidx.navigation:navigation-ui", version.ref = "navigation" }
navigationFragmentKtx   = { module = "androidx.navigation:navigation-fragment-ktx", version.ref = "navigation" }
navigationUiKtx         = { module = "androidx.navigation:navigation-ui-ktx", version.ref = "navigation" }
navigationFeature       = { module = "androidx.navigation:navigation-dynamic-features-fragment", version.ref = "navigation" }
navigationSafeArgs      = { module = "androidx.navigation:navigation-safe-args-gradle-plugin", version.ref = "navigation"}
preferences             = { module = "androidx.preference:preference", version.ref = "preferences" }
media3common            = { module = "androidx.media3:media3-common", version.ref = "media3" }
media3exoplayer         = { module = "androidx.media3:media3-exoplayer", version.ref = "media3" }
media3datasource        = { module = "androidx.media3:media3-datasource", version.ref = "media3" }
media3okhttp            = { module = "androidx.media3:media3-datasource-okhttp", version.ref = "media3" }
media3session           = { module = "androidx.media3:media3-session", version.ref = "media3" }
swipeRefresh            = { module = "androidx.swiperefreshlayout:swiperefreshlayout", version.ref = "swipeRefresh" }
kotlinStdlib            = { module = "org.jetbrains.kotlin:kotlin-stdlib", version.ref = "kotlin" }
kotlinReflect           = { module = "org.jetbrains.kotlin:kotlin-reflect", version.ref = "kotlin" }
kotlinxCoroutines       = { module = "org.jetbrains.kotlinx:kotlinx-coroutines-android", version.ref = "kotlinxCoroutines" }
kotlinxGuava            = { module = "org.jetbrains.kotlinx:kotlinx-coroutines-guava", version.ref = "kotlinxCoroutines"}
retrofit                = { module = "com.squareup.retrofit2:retrofit", version.ref = "retrofit" }
gsonConverter           = { module = "com.squareup.retrofit2:converter-gson", version.ref = "retrofit" }
jacksonConverter        = { module = "com.squareup.retrofit2:converter-jackson", version.ref = "retrofit" }
jacksonKotlin           = { module = "com.fasterxml.jackson.module:jackson-module-kotlin", version.ref = "jackson" }
okhttpLogging           = { module = "com.squareup.okhttp3:logging-interceptor", version.ref = "okhttp" }
koinCore                = { module = "io.insert-koin:koin-core", version.ref = "koin" }
koinAndroid             = { module = "io.insert-koin:koin-android", version.ref = "koin" }
koinViewModel           = { module = "io.insert-koin:koin-android-viewmodel", version.ref = "koin" }
picasso                 = { module = "com.squareup.picasso:picasso", version.ref = "picasso" }
timber                  = { module = "com.jakewharton.timber:timber", version.ref = "timber" }
fastScroll              = { module = "com.simplecityapps:recyclerview-fastscroll", version.ref = "fastScroll" }
colorPickerView         = { module = "com.github.skydoves:colorpickerview", version.ref = "colorPicker" }
rxJava                  = { module = "io.reactivex.rxjava3:rxjava", version.ref = "rxJava" }
rxAndroid               = { module = "io.reactivex.rxjava3:rxandroid", version.ref = "rxAndroid" }
multiType               = { module = "com.drakeet.multitype:multitype", version.ref = "multiType" }

junit                   = { module = "junit:junit", version.ref = "junit4" }
junitVintage            = { module = "org.junit.vintage:junit-vintage-engine", version.ref = "junit5" }
kotlinJunit             = { module = "org.jetbrains.kotlin:kotlin-test-junit", version.ref = "kotlin" }
mockitoKotlin           = { module = "org.mockito.kotlin:mockito-kotlin", version.ref = "mockitoKotlin" }
mockito                 = { module = "org.mockito:mockito-core", version.ref = "mockito" }
kluent                  = { module = "org.amshove.kluent:kluent", version.ref = "kluent" }
kluentAndroid           = { module = "org.amshove.kluent:kluent-android", version.ref = "kluent" }
mockWebServer           = { module = "com.squareup.okhttp3:mockwebserver", version.ref = "okhttp" }
apacheCodecs            = { module = "commons-codec:commons-codec", version.ref = "apacheCodecs" }
robolectric             = { module = "org.robolectric:robolectric", version.ref = "robolectric" }
